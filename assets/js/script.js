"use strict";

/**
 * Add a event listener on multiple elements
 * Agregue un detector de eventos en múltiples elementos
 */

const addEventOnElements = function (elements, eventType, callback) {
  for (let i = 0, len = elements.length; i < len; i++) {
    elements[i].addEventListener(eventType, callback);
  }
};

/**
 * MOBILE NAVBAR TOGGLER
 */
/**
 * "toggle" para indicar "activar/desactivar" algo.
 * toggler: alternar
 * toggler: es una función que permite a un usuario de computadora cambiar a una opción, vista, aplicación, etc. diferente.
 */

const navbar = document.querySelector("[data-navbar]");
const navTogglers = document.querySelectorAll("[data-nav-toggler]");

const toggleNav = () => {
  navbar.classList.toggle("active");
  /**
   * esto hace que se active la clase nav-active e impide que se pueda hacer scroll en el eje x y el eje y con un overflow:hidden
   */
  document.body.classList.toggle("nav-active");
};

addEventOnElements(navTogglers, "click", toggleNav);

/**
 * HEADER ANIMATION
 * When scrolled donw to 100px header will be active
 * Cuando se desplace la cabecera a 100px estará activa
 */

const header = document.querySelector("[data-header]");
/**
 * esto es para que el boton te lleve al inicio de la pagina
 */
const backTopBtn = document.querySelector("[data-back-top-btn]");

window.addEventListener("scroll", () => {
  if (window.scrollY > 100) {
    header.classList.add("active");
    /**
     * el boton aparece cuando se pasa el valor 100 en el scrol deleje y
     */
    backTopBtn.classList.add("active");
  } else {
    header.classList.remove("active");
    backTopBtn.classList.remove("active");
  }
});

/**
 * SLIDER
 */

const slider = document.querySelector("[data-slider]");
const sliderContainer = document.querySelector("[data-slider-container]");
const sliderPrevBtn = document.querySelector("[data-slider-prev]");
const sliderNextBtn = document.querySelector("[data-slider-next]");

/**
 * getComputedStyle devuelve un objeto CSSStyleDeclaration.
 * Este objeto tiene el método getPropertyValue con el que podemos
 * obtener el valor de la propiedad CSS que nos interese
 */
let totalSliderVisibleItems = Number(
  getComputedStyle(slider).getPropertyValue("--slider-items")
);
/**
 * https://www.um.es/docencia/barzana/DAWEB/2017-18/daweb-tema-20-js.html
 * sliderContainer.childElementCount = La propiedad de solo lectura Document.childElementCount
 * devuelve el número de elementos secundarios del documento.
 * https://www.w3bai.com/es/jsref/prop_element_childelementcount.html#gsc.tab=0
 */
let totalSlidableItems =
  sliderContainer.childElementCount - totalSliderVisibleItems;

let currentSlidePos = 0;

/**
 * HTMLElement.offsetLeft devuelve el número de píxeles que la esquina superior izquierda del
 * elemento actual está desplazada hacia la izquierda dentro del nodo HTMLElement.offsetParent .
 *
 * https://es.javascript.info/size-and-scroll
 */
const moveSliderItem = function () {
  sliderContainer.style.transform = `translateX(-${sliderContainer.children[currentSlidePos].offsetLeft}px)`;
};

/**
 * NEXT SLIDE
 */

const slideNext = function () {
  // si el currentSlidePos es mayor al total de items del slider comienza de nuevo con el primero
  const slideEnd = currentSlidePos >= totalSlidableItems;

  if (slideEnd) {
    currentSlidePos = 0;
  } else {
    currentSlidePos++;
  }

  moveSliderItem();
};

sliderNextBtn.addEventListener("click", slideNext);

/**
 * PREVIOUS SLIDE
 */

const slidePrev = function () {
  if (currentSlidePos <= 0) {
    currentSlidePos = totalSlidableItems;
  } else {
    currentSlidePos--;
  }

  moveSliderItem();
};

sliderPrevBtn.addEventListener("click", slidePrev);

/**
 * RESPONSIVE
 */
window.addEventListener("resize", function () {
  totalSliderVisibleItems = Number(
    getComputedStyle(slider).getPropertyValue("--slider-items")
  );
  totalSlidableItems =
    sliderContainer.childElementCount - totalSliderVisibleItems;

  moveSliderItem();
});
